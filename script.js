const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random() * 2)
    },
    toString: function() {
        let str = ""
        if(this.state === 0){
            str = "head"
        } else if(this.state === 1){
            str = "tail"
        }
        return str
    },

    toHTML: function() {
        const image = document.createElement('img');
    if (this.state === 0){
        image.style.width = "50px"
        image.style.height = "50px"
        image.src = './images/dimeheads.jpg'

    } else if (this.state === 1){
        image.style.width = "50px"
        image.style.height = "50px"
        image.src = './images/dimetails.jpeg'

    }

        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        return image;
    }
};
function display20Flips() {
    const results = [];

    for (let i = 0; i < 20; i++) {
        coin.flip()
        results.push(coin.toString())
    }
    document.getElementById("display").textContent = JSON.stringify(results)
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
} 
function display20Images() {
    const results = [];
let DOM = document.getElementById("images")

    for ( let i = 1; i < 20; i++){
        coin.flip()

        let images1 = coin.toHTML();

        document.getElementById("images").appendChild(images1)

        results.push(coin.toHTML())
    }
    return results
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
}

document.getElementById("flip").addEventListener("click", function(){
    location.reload()
})

display20Flips()
display20Images()